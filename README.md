**********************************************************Descrição********************************************************** 

O projeto ACMEVita foi desenvolvido como projeto Backend para o processo de seleção da Telavita.

**********************************************************Objetivo********************************************************** 

Criação de uma API utilizando Python e Flask.

******************************************Ferramentas utilizadas no desenvolvimento******************************************

- Pycharm (IDE Python)
- Flask (Microframework)
- SQLAlchemy (ORM)
- Postman (Ambiente de teste de API)


*****************************************************Arquivos do projeto*****************************************************

- telavita.db - sample do banco de dados com alguns registros nas tabelas departamento e colaborador
- criaBanco.py - utilizado para criar o banco de dados telavita.db com alguns registros
- dados.py - camada de dados com a criação da engine, sessão e a estrutura das tabelas
- dbutils.py - funções para inserir e consultar o banco
- run.py - seria a main do projeto, contempla a API criada utlizando flask


**********************************************************Instruções**********************************************************

1. Executar o arquivo run.py

2. Pegar a URL que aparecer na tela após a mensagem:"Running on"
(Exemplo: http://127.0.0.1:5000/ )

3. Para Consultar todos os departamentos, acrescentar após a URL: "/ListarDepartamentos"
(Exemplo da URL para consulta de departamentos: http://127.0.0.1:5000/ListarDepartamentos )

4. Para Consultar todos os colaboradores de um determinado departamento, basta acrescentar após a URL: "/ListarColaboradores/<departamento>". 
No sample do banco de dados temos 3 departamentos: 'TI', 'RH' e 'Financeiro', que devem ser colocados como <departamento> na URL.
(Exemplo da URL para consulta de colaboradores do departamento de TI: http://127.0.0.1:5000/ListarColaboradores/TI)


OBS: As respostas da API estão em formato json.






