from dados import init_db
from dbutils import insere_dep, insere_col

if __name__ == '__main__':
    init_db()
    insere_dep('RH')
    insere_dep('Financeiro')
    insere_dep('TI')
    insere_col('Joao', 0, 1)
    insere_col('Maria', 1, 2)
    insere_col('Jose', 2, 3)
    insere_col('Mauro', 3, 1)
    insere_col('Ana', 0, 2)
    insere_col('Jessica', 1, 3)
    insere_col('Luiza', 2, 1)
    insere_col('Bruno', 3, 2)
    insere_col('Julio', 0, 3)