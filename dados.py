from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.orm import scoped_session, sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///telavita.db')
db_session = scoped_session(sessionmaker(autocommit=False, bind=engine))

base = declarative_base()
base.query = db_session.query_property()

class Departamento(base):
    __tablename__='departamento'
    id_dep = Column(Integer, primary_key=True)
    nome_dep = Column(String(30), index=True)

    def salvar(self):
        db_session.add(self)
        db_session.commit()

class Colaborador(base):
    __tablename__='colaborador'
    id_col = Column(Integer, primary_key=True)
    nome_col = Column(String(50))
    dependentes_col = Column(Integer)
    departamento_col = Column(Integer, ForeignKey('departamento.id_dep'))
    departamento = relationship("Departamento")

    def salvar(self):
        db_session.add(self)
        db_session.commit()

def init_db():
    base.metadata.create_all(bind=engine)