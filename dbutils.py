from dados import Departamento, Colaborador
import json

def insere_dep(nome):
    dep = Departamento(nome_dep=nome)
    dep.salvar()

def insere_col(nome,dependentes,departamento):
    col = Colaborador(nome_col=nome, dependentes_col=dependentes, departamento_col=departamento)
    col.salvar()

def consulta_todos_dep():
    dep = Departamento.query.all()
    return dep

def consulta_todos_col():
    col = Colaborador.query.all()
    return col

def consulta_col_por_dep(departamento):
    dep = Departamento.query.filter_by(nome_dep=departamento).first()
    try:
        col = Colaborador.query.filter_by(departamento_col=dep.id_dep)
        return col
    except:
        return 'Departamento não encontrado!'

