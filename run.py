from flask import Flask
from dbutils import consulta_todos_dep, consulta_col_por_dep
import json

app = Flask(__name__)

@app.route('/ListarDepartamentos', methods=['GET'])
def departamentos():
    listadep=[]
    try:
        for i in consulta_todos_dep():
            listadep.append(i.nome_dep)

        jsonStr = json.dumps(listadep)
        return jsonStr
    except:
        return 'Erro na consulta de Departamentos!'

@app.route('/ListarColaboradores/<departamento>', methods=['GET'])
def colaboradores(departamento):
    listacol=[]
    try:
        for i in consulta_col_por_dep(departamento):
            if i.dependentes_col > 0:
                have_dependents = True
            else:
                have_dependents = False
            listacol.append('{{nome_colaborador:{},have_dependents:{}}}'.format(i.nome_col, have_dependents))

        jsonStr = json.dumps(listacol)
        return jsonStr
    except:
        return 'Erro na consulta de Colaboradores! Certifique-se que o departamento procurado existe!'

if __name__=="__main__":
    app.run(debug=True)

